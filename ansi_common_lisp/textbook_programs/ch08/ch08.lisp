;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 8.1
(symbol-name 'abc)

;; by default, Common Lisp is not case-sensitive
(eql 'aBc 'Abc)		;; => T

(CaR '(a b c))		;; => A

(list '|Lisp 1.5| '|| '|abc| '|ABC|)

;; the vertical bars are a special syntax for denoting symbols, they are not
;; part of the symbol's name
(symbol-name '|a b c|)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 8.2
(get 'alizarin 'color)
(setf (get 'alizarin 'color) 'red)
(get 'alizarin 'color)
(setf (get 'alizarin 'transparency) 'high)
(get 'alizarin 'transparency)

(symbol-plist 'alizarin)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 8.4
(intern "RANDOM-SYMBOL")
(symbol-name (intern "RANDOM-SYMBOL"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 8.5
;; (defpackage "MY-APPLICATION"
;;   (:use "COMMON-LISP" "MY-UTILITIES")
;;   (:nicknames "APP")
;;   (:export "WIN" "LOSE" "DRAW"))

;; (in-package my-application)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 8.8
(defparameter *words* (make-hash-table :size 10000))

(defconstant maxword 100)

(defun read-text (pathname)
  (with-open-file (s pathname :direction :input)
    (let ((buffer (make-string maxword))
          (pos 0))
      (do ((c (read-char s nil :eof)
              (read-char s nil :eof)))
          ((eql c :eof))
        (if (or (alpha-char-p c) (char= c #\'))
            (progn
              (setf (aref buffer pos) c)
              (incf pos))
            (progn
              (unless (zerop pos)
                (see (intern (string-downcase
                              (subseq buffer 0 pos))))
                (setf pos 0))
              (let ((p (punc c)))
                (if p
                    (see )))))))))

(defun punc (c)
  (case c
    (#\. '|.|)
    (#\, '|.|)
    (#\; '|;|)
    (#\! '|!|)
    (#\? '|?|)))

(let ((prev `|.|))
  (defun see (symb)
    (let ((pair (assoc symb (gethash prev *words*))))
      (if (null pair)
          (push (cons symb 1)
                (gethash prev *words*))
          (incf (cdr pair))))
    (setf prev symb)))

(defun generate-text (n &optional (prev '|.|))
  (if (zerop n)
      (terpri)
      (let ((next (random-next prev)))
        (format t "~A " next)
        (generate-text (1- n) next))))

(defun random-next (prev)
  (let ((choices (gethash prev *words*))
        (i (random (reduce #'+ choices
                           :key #'cdr))))
    (dolist (pair choices)
      (if (minusp (decf i (cdr pair)))
          (return (car pair))))))
