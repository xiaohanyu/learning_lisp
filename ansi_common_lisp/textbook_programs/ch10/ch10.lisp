;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 10.1
(eval '(+ 1 2 3))

(eval '(format t "Hello"))

(defun our-toplevel ()
  (do ()
      (nil)
    (format t "~%> ")
    (print (eval (read)))))

(coerce '(lambda (x) x) 'function)
(compile nil '(lambda (x) (+ x 2)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 10.2
(defmacro nil! (x)
  (list 'setf x nil))

(nil! x)

(macroexpand-1 '(nil! x))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 10.3
`(a b c)

(setf a 1 b 2)
`(a is ,a and b is ,b)

(defmacro nil! (x)
  `(setf ,x nil))

(setf lst '(a b c))
`(lst is ,lst)
`(its elements are ,@lst)

(defmacro while (test &rest body)
  `(do ()
       ((not ,test))
     ,@body))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 10.4
(defun quicksort (vec l r)
  (let ((i l)
        (j r)
        (p (svref vec (round (+ l r) 2))))
    (while (<= i j)
      (while (< (svref vec i) p) (incf i))
      (while (> (svref vec j) p) (decf j))
      (when (<= i j)
        (rotatef (svref vec i) (svref vec j))
        (incf i)
        (decf j)))
    (if (> (- j l) 1) (quicksort vec 1 j))
    (if (> (- r i) 1) (quicksort vec i r)))
  vec)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 10.5
;; Writing macros is a distinct kind of programming, with its own unique aims
;; and problems. Being able to change what the compiler sees is almost like
;; being able to rewrite it. So when you start writing macros, you have to start
;; thinking like a language designer.

;; an incorrect ntimes macro
(defmacro ntimes (n &rest body)
  `(do ((x 0 (+ x 1)))
       ((>= x ,n))
     ,@body))

(ntimes 10
        (princ "."))

(let ((x 10))
  (ntimes 5
          (setf x (+ x 1)))
  x)

;; an improved version with gensym
(defmacro ntimes (n &rest body)
  (let ((g (gensym)))
    `(do ((,g 0 (+ ,g 1)))
         ((>= ,g ,n))
       ,@body)))

(ntimes 10
        (princ "."))

(let ((v 10))
  (ntimes (setf v (- v 1))
          (princ ".")))

;; a correct definition of ntimes
(defmacro ntimes (n &rest body)
  (let ((g (gensym))
        (h (gensym)))
    `(let ((,h ,n))
       (do ((,g 0 (+ ,g 1)))
           ((>= ,g ,h))
         ,@body))))

(pprint (macroexpand-1 '(cond
                         (a b)
                         (c d e)
                         (t f))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 10.6
(defmacro cah (lst)
  `(car ,lst))

(let ((x (list 'a 'b 'c)))
  (setf (cah x) 44)
  x)

;; an incorrect incf macro
(defmacro my-incf (x &optional (y 1))
  `(setf ,x (+ ,x ,y)))

(setf (car (push 1 lst)) (1+ (car (push 1 lst))))
(incf (car (push 1 lst)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 10.7
(defmacro for (var start stop &body body)
  (let ((gstop (gensym)))
    `(do ((,var ,start (1+ ,var))
          (,gstop ,stop))
         ((> ,var ,gstop))
       ,@body)))

(for x 1 8
  (princ x))

(defmacro in (obj &rest choices)
  (let ((insym (gensym)))
    `(let ((,insym ,obj))
       (or ,@(mapcar #'(lambda (c) `(eql ,insym ,c))
                     choices)))))

(in 'x 'x 'y 'z)

(defmacro random-choice (&rest exprs)
  `(case (random ,(length exprs))
     ,@(let ((key -1))
            (mapcar #'(lambda (expr)
                        `(,(incf key) ,expr))
                    exprs))))

(random-choice 'turn-left 'turn-right 'no-turn)

(defmacro avg (&rest args)
  `(/ (+ ,@args) ,(length args)))

(avg 1 2 3)

(defmacro with-gensyms (syms &body body)
  `(let ,(mapcar #'(lambda (s)
                     `(,s (gensym)))
                 syms)
     ,@body))

(defmacro aif (test then &optional else)
  `(let ((it ,test))
     (if it ,then ,else)))

;; Is it worth writing a macro just to save typing? Very much so. Saving typeing
;; is what programming languages are all about; the purpose of a compiler is to
;; save you from typing your program in machine language.

;; Ar you're writing a program, ask yourself, am I writing a macroexpansions? If
;; so, the macros that generate those expansions are the ones you need to
;; write. 
