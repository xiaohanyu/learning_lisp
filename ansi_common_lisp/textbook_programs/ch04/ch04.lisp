;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 4.1

(setf arr (make-array '(2 3) :initial-element nil))

(aref arr 0 0)

(setf (aref arr 0 0) 'b)
(aref arr 0 0)

(setf *print-array* t)

(vector "a" 'b 3)
(svref vec 0)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 4.2
(defun bin-search (obj vec)
  (let ((len (length vec)))
    (if (not (zerop len))
        (finder obj vec 0 (- len 1)))))

(defun finder (obj vec start end)
  (format t "~A~%" (subseq vec start (+ end 1)))
  (let ((range (- end start)))
    (if (zerop range)
        (if (eql obj (aref vec start))
            obj
            nil)
        (let ((mid (+ start (round (/ range 2)))))
          (let ((obj2 (aref vec mid)))
            (if (< obj obj2)
                (finder obj vec start (- mid 1))
                (if (> obj obj2)
                    (finder obj vec (+ mid 1) end)
                    obj)))))))

(bin-search 3 #(0 1 2 3 4 5 6 7 8 9))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 4.3
(sort "elbow" #'char<)

(aref "abc" 1)
(char-code #\a)

(char "abc" 1)

(let ((str (copy-seq "Merlin")))
  (setf (char str 3) #\k)
  str)

(equal "fred" "fred")
(equal "fred" "Fred")
(string-equal "fred" "Fred")

(format nil "~A or ~A" "truth" "dare")
(concatenate 'string "not " "to worry")

(defun mirror? (lst)
  (let ((len (length lst)))
    (if (oddp len)
        nil
        (equal (subseq lst 0 (/ len 2))
               (reverse (subseq lst (/ len 2)))))))
(mirror? "abba")
(mirror? "abcba")

(defun vector-mirror? (s)
  (let ((len (length s)))
    (and (evenp len)
         (do ((forward 0 (+ forward 1))
              (back (- len 1) (- back 1)))
             ((or (> forward back)
                  (not (eql (elt s forward)
                            (elt s back))))
              (> forward back))))))

(vector-mirror? #(1 2 3 3 2 1))
(vector-mirror? #(1 2 3 4 3 2 1))

(position #\a "fantasia")
(position #\a "fantasia" :start 3 :end 5)
(position #\a "fantasia" :from-end t)
(position 'a '((c d) (a b)) :key #'car)
(position '(a b) '((a b) (c d)))
(position '(a b) '((a b) (c d)) :test #'equal)
(position 3 '(1 0 7 5) :test #'<)

(defun second-word (str)
  (let ((p1 (+ (position #\  str) 1)))
    (subseq str p1 (position #\  str :start p1))))

(second-word "Form follows function.")

(position-if #'oddp '(2 3 4 5))

;; the function member cannot apply to a string object
(find #\a "cat")
(find-if #'characterp "ham")

(find-if #'(lambda (x)
             (eql (car x) 'complete))
         '((a b) (c d) (e f) (complete)))

;; is remove-duplicates one-pass or two-pass?
;; in my opinion, it needs a one-pass scanning and a priority queue. 
(remove-duplicates "abracadara")

(reduce #'intersection '((b r a d 's) (b a d) (c a t)))
(intersection '(a b) nil)	;; ==> nil


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 4.5

(defun tokens (str test start)
  (let ((p1 (position-if test str :start start)))
    (if p1
        (let ((p2 (position-if #'(lambda (c)
                                   (not (funcall test c)))
                               str :start p1)))
          (cons (subseq str p1 p2)
                (if p2
                    (tokens str test p2)
                    nil))))))

(defun constituent (c)
  (and (graphic-char-p c)
       (not (char= c #\ ))))

(tokens "ab12 3cde.f" #'alpha-char-p 0)
(tokens "ab12 3cde.f gh" #'constituent 0)

(defun parse-date (str)
  (let ((toks (tokens str #'constituent 0)))
    (list (parse-integer (first toks))
          (parse-month (second toks))
          (parse-integer (third toks)))))

(defconstant month-names
  #("jan" "feb" "mar" "apr" "may" "jun"
    "jul" "aug" "sep" "oct" "nov" "dec"))

(defun parse-month (str)
  (let ((p (position str month-names
                     :test #'string-equal)))
    (if p
        (+ p 1)
        nil)))

(parse-date "16 Aug 1980")

(defun read-integer (str)
  (if (every #'digit-char-p str)
      (let ((accum 0))
        (dotimes (pos (length str))
          (setf accum (+ (* accum 10)
                         (digit-char-p (char str pos)))))
        accum)
      nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 4.6
(defstruct point
  x
  y)

(setf p (make-point :x 0
                    :y 0))

(point-x p)
(setf (point-y p) 2)
(point-p p)
(typep p 'point)

(defstruct polemic
  (type (progn
          (format t "What kind of polemic was it?")
          (read)))
  (effect nil))

(make-polemic)

(defstruct (point (:conc-name p)
                  (:print-function print-point))
  (x 0)
  (y 0))

(defun print-point (p stream depth)
  (format stream "#<~A, ~A>" (px p) (py p)))

(make-point)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 4.7
;;; A binary search tree in lisp, great!
(defstruct (node (:print-function
                  (lambda (n s d)
                    (format s "#<~A>" (node-elt n)))))
  elt (l nil) (r nil))

(defun bst-insert (obj bst <)
  (if (null bst)
      (make-node :elt obj)
      (let ((elt (node-elt bst)))
        (if (eql obj elt)
            bst)
        (if (funcall < obj elt)
            (make-node
             :elt elt
             :l (bst-insert obj (node-l bst) <)
             :r (node-r bst))
            (make-node
             :elt elt
             :l (node-l bst)
             :r (bst-insert obj (node-r bst) <))))))

(defun bst-find (obj bst <)
  (if (null bst)
      nil
      (let ((elt (node-elt bst)))
        (if (eql obj elt)
            bst
            (if (funcall < obj elt)
                (bst-find obj (node-l bst) <)
                (bst-find obj (node-r bst) <))))))

(defun bst-min (bst)
  (and bst
       (or (bst-min (node-l bst))
           bst)))

(defun bst-max (bst)
  (and bst
       (or (bst-max (node-r bst))
           bst)))

(defun bst-remove (obj bst <)
  (if (null bst)
      nil
      (let ((elt (node-elt bst)))
        (if (eql obj elt)
            (percolate bst)
            (if (funcall < obj elt)
                (make-node
                 :elt elt
                 :l (bst-remove obj (node-l bst) <)
                 :r (node-r bst))
                (make-node
                 :elt elt
                 :l (node-l bst)
                 :r (bst-remove obj (node-r bst) <)))))))

(defun percolate (bst)
  (cond ((null (node-l bst))
         (if (null (node-r bst))
             nil
             (rperc bst)))
        ((null (node-r bst))
         (lperc bst))
        (t (if (zerop (random 2))
               (lperc bst)
               (rperc bst)))))

(defun rperc (bst)
  (make-node :elt (node-elt (node-r bst))
             :l (node-l bst)
             :r (percolate (node-r bst))))


(defun lperc (bst)
  (make-node :elt (node-elt (node-l bst))
             :l (percolate (node-l bst))
             :r (node-r bst)))

;; in-order traversal
(defun bst-traverse (fn bst)
  (when bst
    (bst-traverse fn (node-l bst))
    (funcall fn (node-elt bst))
    (bst-traverse fn (node-r bst))))

(setf nums nil)
(dolist (x '(5 8 4 2 1 9 6 7 3))
  (setf nums (bst-insert x nums #'<)))

(bst-min nums)
(bst-max nums)

(bst-traverse #'(lambda (x)
                  (format t "~A " x))
              nums)

(bst-remove 2 nums #'<)
(bst-traverse #'princ nums)

(bst-traverse #'princ (bst-remove 2 nums #'<))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 4.8
(setf ht (make-hash-table))

(gethash 'color ht)
(setf (gethash 'color ht) 'red)
(gethash 'color ht)

(defun our-member (obj lst)
  (if (null lst)
      nil
      (if (eql (car lst) obj)
          lst
          (our-member obj (cdr lst)))))

(our-member 'a '(b a c d))

(setf bugs (make-hash-table))
(push "Doesn't take keyword arguments"
      (gethash #'our-member bugs))

(setf fruit (make-hash-table))
(setf (gethash 'apricot fruit) t)
(gethash 'apricot fruit)
(remhash 'apricot fruit)

(setf (gethash 'shape ht) 'spherical
      (gethash 'size ht) 'giant)

(maphash #'(lambda (k v)
             (format t "~A = ~A~%" k v))
         ht)

(make-hash-table :size 5)

(setf writers (make-hash-table :test #'equal))
(setf (gethash '(ralph waldo emerson) writers) t)
(gethash '(ralph waldo emerson) writers)
