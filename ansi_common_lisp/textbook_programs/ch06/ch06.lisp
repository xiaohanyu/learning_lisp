;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 6.1
(fboundp '+)

(symbol-function '+)

(setf (symbol-function 'add2)
      #'(lambda (x) (+ x 2)))
(add2 1)

(defun add2 (x) (+ x 2))

;; by making the first argument to defun a list of the form (setf f), you define
;; what happens when the first argument to setf is a call to f
(defun primo (lst) (car lst))

(defun (setf primo) (val lst)
  (setf (car lst) val))

(let ((x (list 'a 'b 'c)))
  (setf (primo x) 480)
  x)

(documentation 'defun 'function)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 6.2
(labels ((add10 (x) (+ x 10))
         (consa (x) (cons 'a x)))
  (consa (add10 3)))

(labels ((len (lst)
           (if (null lst)
               0
               (+ (len (cdr lst)) 1))))
  (len '(a b c)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 6.3
(defun our-funcall (fn &rest args)
  (apply fn args))

(our-funcall #'+ 1 2 3)

(defun philosoph (thing &optional property)
  (list thing 'is property))
(philosoph 'death)

(defun philosoph (thing &optional (property 'fun))
  (list thing 'is property))
(philosoph 'death)

(defun keylist (a &key x y z)
  (list a x y z))

(keylist 1 :y 2)
(keylist 1 :y 3 :x 2)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 6.4
(defun single? (lst)
  (and (consp lst)
       (null (cdr lst))))

(defun append1 (lst obj)
  (append lst (list obj)))

(defun map-int (fn n)
  (let ((acc nil))
    (dotimes (i n)
      (push (funcall fn i) acc))
    (nreverse acc)))


(defun filter (fn lst)
  (let ((acc nil))
    (dolist (x lst)
      (let ((val (funcall fn x)))
        (if val
            (push val acc))))
    (nreverse acc)))


(defun most (fn lst)
  (if (null lst)
      (values nil nil)
      (let* ((wins (car lst))
             (max (funcall fn wins)))
        (dolist (obj (cdr lst))
          (let ((score (funcall fn obj)))
            (when (> score max)
              (setf wins obj
                    max score))))
        (values wins max))))

;; test
(single? '(a))

(append1 '(a b c) 'd)

(map-int #'identity 10)
(map-int #'(lambda (x) (random 100))
         10)

(filter #'(lambda (x)
            (and (evenp x) (+ x 10)))
        '(1 2 3 4 5 6 7))

(most #'length '((a b) (a b c) (a)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 6.5

;; impressive function, the Runtime Type Info is useful(C++'s RTTI?)
(defun combiner (x)
  (typecase x
    (number #'+)
    (list #'append)
    (t #'list)))

(defun combine (&rest args)
  (apply (combiner (car args))
         args))

(combine 2 3)
(combine '(a b) '(c d))

(setf fn (let ((i 3))
           #'(lambda (x) (+ x i))))

(funcall fn 2)

(defun add-to-list (num lst)
  (mapcar #'(lambda (x)
              (+ x num))
          lst))

(add-to-list 2 '(1 2 3 4))

(defun make-adder (n)
  #'(lambda (x)
      (+ x n)))

(setf add3 (make-adder 3))
(funcall add3 2)

(setf add27 (make-adder 27))
(funcall add27 2)

(let ((counter 0))
  ;; will cause SBCL 'package lock' error if using 'reset'
  (defun sreset ()
    (setf counter 0))
  (defun stamp ()
    (setf counter (+ counter 1))))

(list (stamp) (stamp) (sreset) (stamp))


(mapcar (complement #'oddp)
        '(1 2 3 4 5 6))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 6.6
(defun compose (&rest fns)
  (destructuring-bind (fn1 . rest) (reverse fns)
    #'(lambda (&rest args)
        (reduce #'(lambda (v f) (funcall f v))
                rest
                :initial-value (apply fn1 args)))))

(defun disjoin (fn &rest fns)
  (if (null fns)
      fn
      (let ((disj (apply #'disjoin fns)))
        #'(lambda (&rest args)
            (or (apply fn args) (apply disj args))))))

(defun conjoin (fn &rest fns)
  (if (null fns)
      fn
      (let ((conj (apply #'conjoin fns)))
        #'(lambda (&rest args)
            (and (apply fn args) (apply conj args))))))

(defun curry (fn &rest args)
  #'(lambda (&rest args2)
      (apply fn (append args args2))))

(defun rcurry (fn &rest args)
  #'(lambda (&rest args2)
      (apply fn (append args2 args))))


;; test
(mapcar (compose #'list #'round #'sqrt)
        '(4 9 16 25))


(mapcar (disjoin #'integerp #'symbolp)
        '(a "a" 2 3))
;; ==>
(mapcar #'(lambda (x)
            (or (funcall #'integerp x)
                (funcall #'symbolp x)))
        '(a "a" 2 3))

(mapcar (conjoin #'integerp #'oddp)
        '(a "a" 2 3))
;; ==>
(mapcar #'(lambda (x)
            (and (funcall #'integerp x)
                 (funcall #'oddp x)))
        '(a "a" 2 3))

;; @see http://en.wikipedia.org/wiki/Currying for mathematical details 
(curry #'+ 3)
(rcurry #'+ 3)

(funcall (curry #'- 3) 2)
(funcall (rcurry #'- 3) 2)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 6.7
;; lexical scope
(let ((x 10))
  (defun foo ()
    x))

(let ((x 20)) (foo))	;; => 10

(let ((x 10))
  (defun foo ()
    (declare (special x))
    x))

(let ((x 20))
  (declare (special x))
  (foo))

;; Global variables established by calling setf at the toplevel are implicitly
;; special 
(setf x 30)
(foo)

(let ((*print-base* 16))
  (princ 32))
