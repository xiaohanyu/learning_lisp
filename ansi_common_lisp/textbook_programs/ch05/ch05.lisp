;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 5.1

(progn
  (format t "a")
  (format t "b")
  (+ 11 12))

(block head
  (format t "Here we go.")
  (return-from head 'idea)
  (format t "We'll never see this."))

(block nil
  (return 27))

(dolist (x '(a b c d e))
  (format t "~A " x)
  (if (eql x 'c)
      (return 'done)))

(defun foo ()
  (return-from foo 27))

(defun read-integer (str)
  (let ((accum 0))
    (dotimes (pos (length str))
      (let ((i (digit-char-p (char str pos))))
        (if i
            (setf accum (+ (* accum 10) i))
            (return-from read-integer nil))))
    accum))

(tagbody
   (setf x 0)
   top
   (setf x (+ x 1))
   (format t "~A " x)
   (if (< x 10) (go top)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 5.2
(let ((x 7)
      (y 2))
  (format t "Number")
  (+ x y))

((lambda (x y)
   (format t "Number")
   (+ x y))
 7
 2)

(let* ((x 2)
       (y (+ x 1)))
  (+ x y))

(let (x y)
  (append x y))

(destructuring-bind (w (x y) . z) '(a (b c) d e)
  (list w x y z))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 5.3
(setf that 3)
(when (oddp that)
  (format t "Hmm, that's odd.")
  (+ that 1))

;; is equivalent to
(if (oddp that)
    (progn
      (format t "Hmm, that's odd.")
      (+ that 1)))

;; cond
(defun our-member (obj lst)
  (if (atom lst)
      nil
      (if (eql (car lst) obj)
          lst
          (our-member obj (cdr lst)))))

;; could also be defined as
(defun our-member (obj lst)
  (cond ((atom lst) nil)
        ((eql (car lst) obj) lst)
        (t (our-member obj (cdr lst)))))

(cond (99))

(defun month-length (mon)
  (case mon
    ((jan mar may jul aug oct dec) 31)
    ((apr jun sept nov) 30)
    (feb (if (leap-year) 29 28))
    (otherwise "unknown month")))

(case 99 (99))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 5.4
(defun show-squares (start end)
  (do ((i start (+ i 1)))
      ((> i end) 'done)
    (format t "~A ~A~%" i (* i i))))

(show-squares 1 10)

(let ((x 'a))
  (do ((x 1 (+ x 1))
       (y x x))
      ((> x 5))
    (format t "(~A ~A)  " x y)))

(do* ((x 1 (+ x 1))
      (y x x))
     ((> x 5))
  (format t "(~A ~A)  " x y))

(dolist (x '(a b c d) 'done)
  (format t "~A " x))

(dotimes (x 5 x)
  (format t "~A " x))

(mapc #'(lambda (x y)
          (format t "~A ~A  " x y))
      '(hip flip slip)
      '(hop flop slop))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 5.5
(values 'a nil (+ 2 4))

((lambda () ((lambda () (values 1 2)))))

(let ((x (values 1 2)))
  x)

(values)
(let ((x (values)))
  x)

(multiple-value-bind (x y z) (values 1 2 3)
  (list x y z))

(multiple-value-bind (x y z) (values 1 2)
  (list x y z))

(multiple-value-bind (s m h) (get-decoded-time)
  (format nil "~A:~A:~A" h m s))

(multiple-value-call #'+ (values 1 2 3))
(multiple-value-list (values 'a 'b 'c))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 5.6

(defun super ()
  (catch 'abort
    (sub)
    (format t "We'll never see this.")))

(defun sub ()
  (throw 'abort 99))

(progn
  (error "Oops!")
  (format t "After the error."))

(setf x 1)
(catch 'abort
  (unwind-protect
       (throw 'abort 99)
    (setf x 2)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 5.7

(setf mon '(31 28 31 30 31 30 31 31 30 31 30 31))
(apply #'+ mon)

(setf nom (reverse mon))
(setf sums (maplist #'(lambda (x)
                        (apply #'+ x))
                    nom))

(reverse sums)

;; code for Figure 5.1, first half of the code, date->num
(defconstant month
  #(0 31 59 90 120 151 181 212 243 273 304 334 365))

(defconstant yzero 2000)

(defun leap? (y)
  (and (zerop (mod y 4))
       (or (zerop (mod y 400))
           (not (zerop (mod y 100))))))

(defun date->num (d m y)
  (+ (- d 1) (month-num m y) (year-num y)))

(defun month-num (m y)
  (+ (svref month (- m 1))
     (if (and (> m 2) (leap? y))
         1
         0)))

(defun year-num (y)
  (let ((d 0))
    (if (>= y yzero)
        (dotimes (i (- y yzero) d)
          (incf d (year-days (+ yzero i))))
        (dotimes (i (- yzero y) (- d))
          (incf d (year-days (+ y i)))))))

(defun year-days (y)
  (if (leap? y)
      366
      365))

(mapcar #'leap? '(1904 1900 1600))

;; code for Figure 5.2, second half of the code, num->date
(defun num->date (n)
  (multiple-value-bind (y left) (num-year n)
    (multiple-value-bind (m d) (num-month left y)
      (values d m y))))

(defun num-year (n)
  (if (< n 0)
      (do* ((y (- yzero 1) (- y 1))
            (d (- (year-days y)) (- d (year-days y))))
           ((<= d n) (values y (- n d))))
      (do* ((y yzero (+ y 1))
            (prev 0 d)
            (d (year-days y) (+ d (year-days y))))
           ((> d n) (values y (- n prev))))))

(defun num-month (n y)
  (if (leap? y)
      (cond ((= n 59) (values 2 29))
            ((> n 59) (nmon (- n 1)))
            (t (nmon n)))
      (nmon n)))

(defun nmon (n)
  (let ((m (position n month :test #'<)))
    (values m (+ 1 (- n (svref month (- m 1)))))))

(defun date+ (d m y n)
  (num->date (+ (date->num d m y) n)))

;; simple test
;; ==> (15 2 1998)
(multiple-value-list (date+ 17 12 1997 60))
