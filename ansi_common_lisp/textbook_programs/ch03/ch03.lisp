;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 3.1
(setf x (cons 'a nil))
(car x)
(cdr x)

(setf y (list 'a 'b 'c))
(cdr y)

(setf z (list 'a (list 'b 'c) 'd))
(car (cdr z))

(defun our-listp (x)
  (or (null x) (consp x)))

(defun our-atomp (x)
  (not (consp x)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 3.2
(eql (cons 'a nil) (cons 'a nil))

(setf x (cons 'a nil))
(eql x x)

(equal x (cons 'a nil))

;; just for list type
;; as this definition suggests, if some x and y are eql, they are also equal 
(defun our-equal (x y)
  (or (eql x y)
      (and (consp x)
           (consp y)
           (our-equal (car x) (car y))
           (our-equal (cdr x) (cdr y)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 3.3
(setf x '(a b c))
(setf y x)
(eql x y)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 3.5
(defun our-copy-list (lst)
  (if (atom lst)
      lst
      (cons (car lst) (our-copy-list (cdr lst)))))

(setf x '(a b c))
(setf y (our-copy-list x))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 3.5
(defun compress (x)
  (if (consp x)
      (compr (car x) 1 (cdr x))
      x))

(defun compr (elt n lst)
  (if (null lst)
      (list (n-elts elt n))
      (let ((next (car lst)))
        (if (eql next elt)
            (compr elt (+ n 1) (cdr lst))
            (cons (n-elts elt n)
                  (compr next 1 (cdr lst)))))))

(defun n-elts (elt n)
  (if (> n 1)
      (list n elt)
      elt))

(defun uncompress (lst)
  (if (null lst)
      nil
      (let ((elt (car lst))
            (rest (uncompress (cdr lst))))
        (if (consp elt)
            (append (apply #'list-of elt)
                    rest)
            (cons elt rest)))))

(defun list-of (n elt)
  (if (zerop n)
      nil
      (cons elt (list-of (- n 1) elt))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 3.6
(nth 0 '(a b c))

(nthcdr 2 '(a b c))

(defun our-nthcdr (n lst)
  (if (zerop n)
      lst
      (our-nthcdr (- n 1) (cdr lst))))

(last '(a b c))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 3.7
(mapcar #'(lambda (x) (+ x 10))
        '(1 2 3))

(mapcar #'list
        '(a b c)
        '(1 2 3 4))

(maplist #'(lambda (x) x)
         '(a b c))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 3.8
(defun our-copy-tree (tr)
  (if (atom tr)
      tr
      (cons (our-copy-tree (car tr))
            (our-copy-tree (cdr tr)))))

;;; some test result in SBCL 1.0.56
;; CL-USER> (setf x '(a (b c) c))
;; ; 
;; ; caught WARNING:
;; ;   undefined variable: X
;; ; 
;; ; compilation unit finished
;; ;   Undefined variable:
;; ;     X
;; ;   caught 1 WARNING condition
;; (A (B C) C)
;; CL-USER> (eql (cadr (OUR-COPY-LIST x)) (cadr x))
;; T
;; CL-USER> (eql (cadr (OUR-COPY-TREE x)) (cadr x))
;; NIL
;; CL-USER>

(substitute 'y 'x '(and (integerp x) (zerop (mod x 2))))
(subst 'y 'x '(and (integerp x) (zerop (mod x 2))))

;; the power of recursion bring us elegant, beautiful and clean programs 
(defun our-subst (new old tree)
  (if (eql tree old)
      new
      (if (atom tree)
          tree
          (cons (our-subst new old (car tree))
                (our-subst new old (cdr tree))))))

(our-subst 'y 'x '(and (integerp x) (zerop (mod x 2))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 3.9
(defun len (lst)
  (if (null lst)
      0
      (+ 1 (len (cdr lst)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 3.10
(member 'b '(a b c))

(member '(a) '((a) (z)) :test #'equal)
;; (eql 'a 'a)
;; ==> t
;; (eql '(a) '(a))
;; ==> nil

(member 'a '((a b) (c d)) :key #'car)

(member 2 '((1) (2)) :key #'car :test #'equal)
(member 2 '((1) (2)) :test #'equal :key #'car)

(member-if #'oddp '(2 3 4))

(defun our-member-if (fn lst)
  (and (consp lst)
       (if (funcall fn (car lst))
           lst
           (our-member-if fn (cdr lst)))))

(our-member-if #'oddp '(2 3 4))

(adjoin 'b '(a b c))
(adjoin 'z '(a b c))

(union '(a b c) '(c b s))
(intersection '(a b c) '(b b c))
(set-difference '(a b c d e) '(b e))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 3.11
(length '(a b c))

(subseq '(a b c d) 1 2)
(subseq '(a b c d) 1)

(reverse '(a b c))

(defun mirror? (s)
  (let ((len (length s)))
    (if (evenp len)
        (let ((mid (/ len 2)))
          (equal (subseq s 0 mid)
                 (reverse (subseq s mid)))))))

(mirror? '(a b b a))		;; => t
(mirror? '(a b c b a))		;; => nil

(sort '(0 2 1 3 8) #'>)		;; be careful when using sort, since it's destructive.

(defun nthmost (n lst)
  (nth (- n 1)
       (sort (copy-list lst) #'>)))

(nthmost 2 '(0 2 1 3 8))

(every #'oddp '(1 3 5))
(some #'evenp '(1 2 3))

(every #'> '(1 3 5) '(0 2 4))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 3.12
;; (push obj lst) <==> (setf lst (cons obj lst))
;; (pop lst) <==>
;; (let ((x (car lst)))
;;   (setf lst (cdr lst))
;;   x)
(setf x '(b))
(push 'a x)
x

(setf y x)
(pop x)
x
y

(defun our-reverse (lst)
  (let ((acc nil))
    (dolist (elt lst)
      (push elt acc))
    acc))

(let ((x '(a b)))
  (pushnew 'c x)
  (pushnew 'a x)
  x)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 3.13
(defun proper-list? (x)
  (or (null x)
      (and (consp x)
           (proper-list? (cdr x)))))

(setf pair (cons 'a 'b))
'(a . (b . (c . nil)))
(proper-list? '(a b c . d))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 3.14
;; assoc-list are slow, but convenient in the first stages of a program
(setf trans '((+ . "add") (- . "subtract")))
(assoc '+ trans)
(assoc '* trans)

(defun our-assoc (key alist)
  (and (consp alist)
       (let ((pair (car alist)))
         (if (eql key (car pair))
             pair
             (our-assoc key (cdr alist))))))

(our-assoc '+ trans)
(our-assoc '* trans)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 3.15
(setf min '((a b c) (b c) (c d e)))

(defun shortest-path (start end net)
  (bfs end (list (list start)) net))

(defun bfs (end queue net)
  (if (null queue)
      nil
      (let ((path (car queue)))
        (let ((node (car path)))
          (if (eql node end)
              (reverse path)
              (bfs end
                   (append (cdr queue)
                           (new-paths path node net))
                   net))))))

(defun new-paths (path node net)
  (mapcar #'(lambda (n)
              (cons n path))
          (cdr (assoc node net))))

(cdr (assoc 'a min))

(shortest-path 'a 'd min)
