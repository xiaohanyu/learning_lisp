;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 9.1
(integerp 1)
(floatp 253.72)
(complexp #c(1 2))

(+ #c(0 1.0) 2)
(/ 10 2)
(+ #c(1 -1) #c(2 1))
(rationalp 2)

;; I didn't find ratiop function, so a defun a new one
(defun ratiop (n)
  (and (rationalp n)
       (not (integerp n))))

(list (ratiop (/ 2 2)) (complexp #c(1 0)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 9.2
(mapcar #'float '(1 2/3 .5))

(truncate 1.3)

(defun palindrome? (x)
  (let ((mid (/ (length x) 2)))
    (equal (subseq x 0 (floor mid))
           (reverse (subseq x (ceiling mid))))))

(palindrome? "abcdcba")
(palindrome? "abcdcbae")

(defun our-truncate (n)
  (if (> n 0)
      (floor n)
      (ceiling n)))

(our-truncate 3.5)
(our-truncate -3.5)

(mapcar #'round '(-2.5 -1.5 1.5 2.5))

(mod 5 2)
(rem 5 2)

;; (* (abs x) (signum x)) = x
(mapcar #'signum '(-2 -0.0 0.0 0 .5 3))

;; numerator and denominator function
(list (numerator 2/3) (denominator 2/3))

;; realpart and imagpart function
(list (realpart #c(2 3)) (imagpart #c(2 3)))

(random 2)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 9.3
(= 1 1.0)		;; => T
(eql 1 1.0)		;; => NIL

(let ((x 0)
      (y 1)
      (z 2))
  (< x y z)
  (/= x y z))

(list (minusp -0.0) (zerop -0.0))

(list (max 1 2 3 4 5) (min 1 2 3 4 5))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 9.4
(1- 3)
(1+ 3)

(* 3) 		;; => 3
(/ 3)		;; => 1/3

(float 365/12)
(float (/ 365 12))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 9.5
(expt 2 5)
(log 32 2)
(exp 2)
(log 7.389056)

(expt 27 1/3)
(sqrt 4)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 9.6
(let ((x (/ pi 4)))
  (list (sin x) (cos x) (tan x)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 9.7
(values most-positive-fixnum most-negative-fixnum)

(typep 1 'fixnum)				;; => T
(typep most-positive-fixnum 'bignum)		;; => NIL
(typep (1+ most-positive-fixnum) 'bignum)	;; => T


;; SBCL: arithmetic error FLOATING-POINT-OVERFLOW signalled
;; (* most-positive-long-float 10)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; section 9.8
;;; the impressive ray-tracing algorithm

(defun sq (x) (* x x))

(defun mag (x y z)
  (sqrt (+ (sq x) (sq y) (sq z))))

(defun unit-vector (x y z)
  (let ((d (mag x y z)))
    (values (/ x d) (/ y d) (/ z d))))

(defstruct (point (:conc-name nil))
  x y z)

(defun distance (p1 p2)
  (mag (- (x p1) (x p2))
       (- (y p1) (y p2))
       (- (z p1) (z p2))))

(defun minroot (a b c)
  (if (zerop a)
      (/ (- c) b)
      (let ((disc (- (sq b) (* 4 a c))))
        (unless (minusp disc)
          (let ((discrt (sqrt disc)))
            (min (/ (+ (- b) discrt) (* 2 a))
                 (/ (- (- b) discrt) (* 2 a))))))))

(defstruct surface color)

(defparameter *world* nil)
(defconstant eye (make-point :x 0 :y 0 :z 200))

(defun tracer (pathname &optional (res 1))
  (with-open-file (p pathname :direction :output)
    (format p "P2 ~A ~A 255" (* res 100) (* res 100))
    (let ((inc (/ res)))
      (do ((y -50 (+ y inc)))
          ((< (- 50 y) inc))
        (do ((x -50 (+ x inc)))
            ((< (- 50 x) inc))
          (print (color-at x y) p))))))

(defun color-at (x y)
  (multiple-value-bind (xr yr zr)
      (unit-vector (- x (x eye))
                   (- y (y eye))
                   (- 0 (z eye)))
    (round (* (sendray eye xr yr zr) 255))))

(defun sendray (pt xr yr zr)
  (multiple-value-bind (s int) (first-hit pt xr yr zr)
    (if s
        (* (lambert s int xr yr zr) (surface-color s))
        0)))

(defun first-hit (pt xr yr zr)
  (let (surface hit dist)
    (dolist (s *world*)
      (let ((h (intersect s pt xr yr zr)))
        (when h
          (let ((d (distance h pt)))
            (when (or (null dist) (< d dist))
              (setf surface s hit h dist d))))))
    (values surface hit)))

(defun lambert (s int xr yr zr)
  (multiple-value-bind (xn yn zn) (normal s int)
    (max 0 (+ (* xr xn) (* yr zn) (* zr zn)))))

(defstruct (sphere (:include surface))
  radius center)

(defun defsphere (x y z r c)
  (let ((s (make-sphere
            :radius r
            :center (make-point :x x :y y :z z)
            :color c)))
    (push s *world*)
    s))

(defun intersect (s pt xr yr zr)
  (funcall (typecase s (sphere #'sphere-intersect))
           s pt xr yr zr))

(defun sphere-intersect (s pt xr yr zr)                                             
  (let* ((c (sphere-center s))                                                      
         (n (minroot (+ (sq xr) (sq yr) (sq zr))                                    
                     (* 2 (+ (* (- (x pt) (x c)) xr)                                
                             (* (- (y pt) (y c)) yr)                                
                             (* (- (z pt) (z c)) zr)))                              
                     (+ (sq (- (x pt) (x c)))                                       
                        (sq (- (y pt) (y c)))                                       
                        (sq (- (z pt) (z c)))                                    
                        (- (sq (sphere-radius s)))))))                           
    (if n                                                                        
        (make-point :x (+ (x pt) (* n xr))                                       
                    :y (+ (y pt) (* n yr))                                       
                    :z (+ (z pt) (* n zr))))))                                   

(defun normal (s pt)                                                             
  (funcall (typecase s (sphere #'sphere-normal))                                 
           s pt))                                  

(defun sphere-normal (s pt)                                                         
  (let ((c (sphere-center s)))                                                      
    (unit-vector (- (x c) (x pt))                                                   
                 (- (y c) (y pt))                                                   
                 (- (z c) (z pt)))))

(defun ray-test (&optional (res 1))                                              
  (setf *world* nil)                                                             
  (defsphere 0 -300 -1200 200 .8)                                                
  (defsphere -80 -150 -1200 200 .7)                                              
  (defsphere 70 -100 -1200 200 .9)                                               
  (do ((x -2 (1+ x)))                                                            
      ((> x 2))                                                                  
    (do ((z 2 (1+ z)))                                                           
        ((> z 7))                                                                
      (defsphere (* x 200) 300 (* z -400) 40 .75)))                              
  (tracer (make-pathname :name "spheres.pgm") res))
