;; use :from-end and :initial-value keyword parameter of reduce function
;; (reduce #'cons '(a b c) :from-end t :initial-value nil)
;; ==> (cons a (cons b (cons c nil)))
;; if without (:initial-value nil), then the result will be 
;; ==> (cons a (cons b c)) ==> (a b . c)
(defun my-copy-list (lst1)
  (let ((lst2 (reduce #'cons lst1 :from-end t :initial-value nil)))
    lst2))

;; use rcons and reduce 
(defun my-reverse (lst1)
  (let ((lst2 (reduce #'rcons lst1 :initial-value nil)))
    lst2))

;; define a reverse cons 
(defun rcons (a b)
  (cons b a))