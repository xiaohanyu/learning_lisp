;; assume that the tree is a proper ternary error, 
;; if not, the redundant tree node is thrown out

(defun copy-ternary-tree (ternary-tree)
  (if (atom ternary-tree)
      ternary-tree
      (let ((tl (length ternary-tree)))
        (cond ((= tl 1)
               (copy-ternary-tree (nth 0 ternary-tree)))
              ((= tl 2)
               (list (copy-ternary-tree (nth 0 ternary-tree))
                     (copy-ternary-tree (nth 1 ternary-tree))))
              ((= tl 3)
               (list (copy-ternary-tree (nth 0 ternary-tree))
                     (copy-ternary-tree (nth 1 ternary-tree))
                     (copy-ternary-tree (nth 2 ternary-tree))))))))


;; this method is a little complicated
;; it need to be constructed by defstructs and defmacros

(defun find-ternary-tree (ternary-tree obj)
  (if (and (atom ternary-tree) (not (eql ternary-tree obj)))
      nil
      (or (eql ternary-tree obj)
          (let ((tl (length ternary-tree)))
            (cond ((= tl 1)
                   (find-ternary-tree (nth 0 ternary-tree) obj))
                  ((= tl 2)
                   (or (find-ternary-tree (nth 0 ternary-tree) obj)
                       (find-ternary-tree (nth 1 ternary-tree) obj)))
                  ((= tl 3)
                   (or (find-ternary-tree (nth 0 ternary-tree) obj)
                       (find-ternary-tree (nth 1 ternary-tree) obj)
                       (find-ternary-tree (nth 2 ternary-tree) obj))))))))