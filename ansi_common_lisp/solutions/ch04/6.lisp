(defun hash-to-alist (ht)
  (let ((alst nil))
    (maphash #'(lambda (k v)
                 (push (cons k v) alst))
             ht)
    alst))

(defun alist-to-hash (alst)
  (let ((ht (make-hash-table)))
    (mapcar #'(lambda (x)
                (setf (gethash (car x) ht)
                      (cdr x)))
            alst)
    ht))

;;; test
;; CL-USER> (setf alist '((+ . "add") (- . "subtract") (* . "multiply")))
;; ((+ . "add") (- . "subtract") (* . "multiply"))
;; CL-USER> (alist-to-hash alist)
;; #<HASH-TABLE :TEST EQL :COUNT 3 {1006D26943}>
;; CL-USER> (hash-to-alist (alist-to-hash alist))
;; ((* . "multiply") (- . "subtract") (+ . "add"))
;; CL-USER> (maphash #'(lambda (k v) (format t "~A:~A~%" k v)) (alist-to-hash alist))
;; +:add
;; -:subtract
;; *:multiply
;; NIL
;; CL-USER> 
