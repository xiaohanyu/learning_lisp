;; (append nil '(1) nil) ==> nil
;; using return values instead modify function parameters directly in lisp
(defun bst-to-list (bst)
  (when bst
    (append
     (bst-to-list (node-l bst))
     (list (node-elt bst))
     (bst-to-list (node-r bst)))))

;; This is an INCORRECT implementation
;; (defun bst-to-list (bst)
;;   (bst-to-list-helper bst nil))

;; (defun bst-to-list (bst lst)
;;   (when bst
;;     (bst-to-list-helper (node-l bst) lst)
;;     (push (node-elt bst) lst)
;;     (bst-to-list-helper (node-r bst) lst)))
