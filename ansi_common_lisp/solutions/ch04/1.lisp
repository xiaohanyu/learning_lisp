;; a[ij] = b[mn]
;; zero-indexed array
;; ==> m = j
;; ==> n + i = dimension - 1 

(defun quarter-turn (square-array)
  (let ((d0 (array-dimension square-array 0))
        (d1 (array-dimension square-array 1))
        (turn-array (copy-array square-array)))
    (if (not (= d0 d1))
        (progn
          (format t "Error! The array is not a square array")
          nil)
        (progn 
          (dotimes (i d0)
            (dotimes (j d0)
              ;;(format t "i: ~a j: ~a~%" i j)))
              (setf (aref turn-array i j) (aref square-array (- d0 j 1) i))))
          turn-array))))

;; from http://lemonodor.com/archives/000100.html
;; need a deep understanding of common lisp array, such as
;; - adjustable
;; - fill-pointer
;; - make-array
;; - adjust-array
;; - initial-contents
;; - zero-dimensional array
;; - displace-to 
;; and so on
(defun copy-array (array)
  (let ((dims (array-dimensions array)))
    (adjust-array
     (make-array dims :displaced-to array)
     dims)))
