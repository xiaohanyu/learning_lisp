;; we define a new-if using cond
(defmacro new-if (test result1 &optional (result2 nil))
  `(cond (,test ,result1)
         (t ,result2)))