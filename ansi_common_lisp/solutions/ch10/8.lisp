;; what means double?

(defmacro double (x)
  (let ((xx (gensym)))
    `(let ((,xx ,x))
       (setf ,x
             (typecase ,xx
               (integer (* ,xx 2))
               (list (append ,xx ,xx))
               (vector (concatenate 'vector ,xx ,xx))
               (t (format t "unkown sequence type~%")))))))

(let ((x 1)
      (y #(2 3))
      (z '(4 5)))
  (double x)
  (double y)
  (double z)
  (list x y z))
