;; Initially I want to implemente n-of using recursion, but failed.
;; @see "On Lisp" section 10.4 about the details about recursion and lisp macros

(defmacro n-of (n expr)
  (let ((nn (gensym))
        (ii (gensym))
        (exprs (gensym)))
    `(let ((,nn ,n)
           (,exprs nil))
       (dotimes (,ii ,nn)
         ;; (format t "~A~%" ,exprs)
         (push ,expr ,exprs))
       (reverse ,exprs))))

(let ((i 0) (n 4))
  (n-of n (incf i)))
