;;; backquoted expressions
(setf x 'a)
(setf y 'b)
(setf z '(c d))

;; ==> ((C D) A Z)
`(,z ,x ,y)

;; ==> (X B C D)
`(x ,y ,@z)

;; ==> ((C D A) Z)
`((,@z ,x) z)