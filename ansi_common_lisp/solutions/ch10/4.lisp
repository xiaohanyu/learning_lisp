;; define ntimes(page 167) to expand into a (local) recursive functions instead
;; of do

(defmacro ntimes (n &rest body)
  `(ntimes-for ,n ',body))

(defun ntimes-for (n body)
  (if (= n 0)
      nil
      (progn
        (mapcar #'(lambda (c)
                    (eval c))
                body)
        (funcall #'ntimes-for (- n 1) body))))

(ntimes 5 (princ "."))
(ntimes 5 (princ "hello") (print " world"))
