;; an improved implementation
;; which is similar to random-choice macro in page 170
(defmacro nth-expr (n &rest exprs)
  `(case (- ,n 1)
     ,@(let ((key -1))
            (mapcar #'(lambda (expr)
                        `(,(incf key) ,expr))
                    exprs))))
       
(let ((n 2))
  (nth-expr n (/ 1 0) (+ 1 2) (/ 1 0)))

(nth-expr 2 (/ 1 0) (+ 1 2) (/ 1 0))
