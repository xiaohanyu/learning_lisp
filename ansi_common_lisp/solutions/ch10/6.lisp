;; An elegant solution from http://www.shido.info/lisp/pacl2_e.html#macro, I
;; myself cannot solve this exercise
(defmacro retain (parms &body body)
  `((lambda ,parms ,@body) ,@parms))

(let ((a 0) (b 1) (c 2) (d 3))
  (format t "values before retain: a=~A, b=~A, c=~A, d=~A~%" a b c d)
  (retain (a b c)      ;retain a b c. not d.
          (setf a (* a 10)
                b (* b 10)
                c (* c 10)
                d (* d 10))
          (format t "values in retain: a=~A, b=~A, c=~A, d=~A~%" a b c d))
  (format t "values after retain: a=~A, b=~A, c=~A, d=~A~%" a b c d))
