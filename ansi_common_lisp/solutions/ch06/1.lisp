(defun constituent (c)
  (and (graphic-char-p c)
       (not (char= c #\ ))))


(defun tokens (str &key (test #'constituent) (start 0))
  (let ((p1 (position-if test str :start start)))
    (if p1
        (let ((p2 (position-if #'(lambda (c)
                                  (not (funcall test c)))
                   str :start p1)))
          (cons (subseq str p1 p2)
                (if p2
                    (tokens str :test test :start p2)
                    nil)))
        nil)))

;; test
(tokens "ab12 3cde.f gh")
(tokens "ab12 3cde.f gh" :test #'alpha-char-p)
(tokens "ab12 3cde.f gh" :test #'alphanumericp)
(tokens "ab12 3cde.f gh" :test #'alphanumericp :start 6)
(tokens "ab12 3cde.f gh" :test #'(lambda (c) (and (char< c #\k)
                                                  (char> c #\b))))
