(defun bin-search (vec &key key (test #'eql) (start 0) (end (- (length vec) 1)))
  ;; (format t "start: ~A \t end: ~A~%" start end)
  (if (>= end start)
      (finder key vec start end test)
      nil))


(defun finder (obj vec start end pred)
  (let ((range (- end start)))
    (cond ((< range 0)
           nil)
          ((zerop range)
           (if (funcall pred obj (aref vec start))
               obj
               nil))
          (t
           (let* ((mid (+ start (round (/ range 2))))
                  (obj2 (aref vec mid)))
             (if (< obj obj2)
                 (finder obj vec start (- mid 1) pred)
                 (if (> obj obj2)
                     (finder obj vec (+ mid 1) end pred)
                     obj)))))))


;; the original debug code
;; (let ((x 0))  
;;   (defun finder (obj vec start end pred)
;;     (if (= x 10)
;;         'done
;;         (progn
;;           (format t "x: ~X~%" x)
;;           (setf x (1+ x))
;;           (format t "start: ~a\tend: ~a~%" start end)
;;           (let ((range (- end start)))
;;             (if (< range 0)
;;                 (format t "below 0~%")
;;                 nil)
;;             (if (zerop range)
;;                 (if (funcall pred obj (aref vec start))
;;                     obj
;;                     nil)
;;                 (let* ((mid (+ start (round (/ range 2))))
;;                        (obj2 (aref vec mid)))
;;                   (format t "mid: ~a~%" mid)
;;                   (if (< obj obj2)
;;                       (finder obj vec start (- mid 1) pred)
;;                       (if (> obj obj2)
;;                           (finder obj vec (+ mid 1) end pred)
;;                           obj)))))))))

(bin-search #(0 1 2 3 4 5 6 7 8 9) :key 3)		;; => 3
(bin-search #(0 1 2 3 4 5 6 7 8 9) :key 3 :start 4)	;; => nil
(bin-search #(0 1 2 3 4 5 6 7 8 9) :key 3 :end 4)	;; => 3
