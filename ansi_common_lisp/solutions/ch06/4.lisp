(defun insert2 (obj fn maxcons)
  (let ((max1 (car maxcons))
        (max2 (cdr maxcons)))
    ;; (format t "max1: ~A \t max2: ~A~%" max1 max2)
    ;; (format t "fn obj: ~A \t fn max1: ~A \t fn max2: ~A \t~%"
    ;;         (funcall fn obj)
    ;;         (funcall fn max1)
    ;;         (funcall fn max2))
    (cond ((or (null max1)
               (> (funcall fn obj)
                  (funcall fn max1)))
           (cons obj max1))
          ((and (<= (funcall fn obj)
                    (funcall fn max1))
                (or (null max2)
                    (> (funcall fn obj)
                       (funcall fn max2))))
           (cons max1 obj))
          (t
           maxcons))))

(defun most (fn lst)
  (if (null lst)
      (values nil nil)
      (let ((maxcons (cons nil nil)))
        (dolist (obj lst)
          (setf maxcons (insert2 obj fn maxcons)))
        (values (car maxcons) (cdr maxcons)))))


;; test
(most #'length '((a) (b c) (d e f) (g h) (i j k l m))) 	;; => (I J K L M), (D E F)
