;; using closure to save some computation results in order to prevent repetitive
;; computation. 
(defun expensive (n)
  ;; (format t "expensive called~%")
  ;; the loop represent the expensive computation ^_^
  (dotimes (i 10000000)
    (* i 1087131))
  n)

(let ((vec (make-array 101 :initial-element nil)))
  (defun frugal (n)
    ;; (format t "vec: ~A~%" vec)
    (let ((val (svref vec n)))
      (if val
          val
          (let ((newn (expensive n)))
            ;; (format t "newn: ~A~%" newn)
            (setf (svref vec n) newn)
            newn)))))

;; test using SBCL's builtin time function
;; CL-USER> (time (frugal 1))
;; Evaluation took:
;;   0.009 seconds of real time
;;   0.009999 seconds of total run time (0.009999 user, 0.000000 system)
;;   111.11% CPU
;;   21,841,568 processor cycles
;;   0 bytes consed
  
;; 1
;; CL-USER> (time (frugal 1))
;; Evaluation took:
;;   0.000 seconds of real time
;;   0.000000 seconds of total run time (0.000000 user, 0.000000 system)
;;   100.00% CPU
;;   2,008 processor cycles
;;   0 bytes consed
  
;; 1
;; CL-USER> (time (frugal 2))
;; Evaluation took:
;;   0.011 seconds of real time
;;   0.009999 seconds of total run time (0.009999 user, 0.000000 system)
;;   90.91% CPU
;;   25,726,376 processor cycles
;;   0 bytes consed
  
;; 2
;; CL-USER> (time (frugal 2))
;; Evaluation took:
;;   0.000 seconds of real time
;;   0.000000 seconds of total run time (0.000000 user, 0.000000 system)
;;   100.00% CPU
;;   1,476 processor cycles
;;   0 bytes consed
  
;; 2
