(defun num-of-args (&rest args)
  (length args))

;; test
(num-of-args 'a 'b 1 2 '(d e f)) 	;; => 5
