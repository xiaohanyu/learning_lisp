(let ((x nil))
  (defun greater-than-last (n)
    (let ((oldx x))
      (setf x n)
      (if (null oldx)
          nil
          (if (> x oldx)
              t
              nil)))))
;; test
;; CL-USER> (greater-than-last 3)
;; NIL
;; CL-USER> (greater-than-last 4)
;; T
;; CL-USER> (greater-than-last 2)
;; NIL
;; CL-USER> (greater-than-last 3)
;; T
;; CL-USER> (greater-than-last 5)
;; T
;; CL-USER> (greater-than-last 4)
;; NIL
;; CL-USER> 
