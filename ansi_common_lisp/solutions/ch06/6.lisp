;; using closure to record the maximum items
(let ((max nil))
  (defun max-so-far (x)
    (if (or (null max)
            (> x max))
        (progn
          (setf max x)
          max)
        max)))

;; test
;; CL-USER> (max-so-far 100)
;; 100
;; CL-USER> (max-so-far 101)
;; 101
;; CL-USER> (max-so-far 9)
;; 101
;; CL-USER> (max-so-far 990)
;; 990
;; CL-USER> (max-so-far 99)
;; 990
;; CL-USER> 
