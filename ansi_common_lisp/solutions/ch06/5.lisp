(defun filter (fn lst)
  (let ((acc nil))
    (dolist (x lst)
      (let ((val (funcall fn x)))
        (if val
            (push val acc))))
    (nreverse acc)))

(filter #'(lambda (x) (+ x 1))
        '(-1 0 1 2 3 4))

(defun my-remove-if (fn lst)
  ;; wrapper fn to ffn, thus filter will push the obj itself instead of t or nil
  (let ((ffn #'(lambda (x)	
                 (if (funcall fn x)
                     nil
                     x))))
    (filter ffn lst)))

;; => (-1 1 3 5)
(my-remove-if (complement #'oddp)
              '(-1 0 1 2 3 4 5))

;; => (-1 1 3 5)
(remove-if #'evenp
           '(-1 0 1 2 3 4 5))

;; => (T T T)
(filter #'evenp
        '(-1 0 1 2 3 4 5))
