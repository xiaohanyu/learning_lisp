;;; 8.a
;; recursive method
(defun recur-print-dots (n)
  (if (zerop n)
      nil
      (progn
        (format t ".")
        (recur-print-dots (- n 1)))))

;; iterative method
(defun iter-print-dots (n)
  (dotimes (i n)
    (format t ".")))

;;; 8.b 
;; recursive method
(defun recur-count (e seq)
  (if (null seq)
      0
      (if (eql e (car seq))
          (1+ (recur-count e (cdr seq)))
          (recur-count e (cdr seq)))))

;; iterative method
(defun iter-count (e seq)
  (if (null seq)
      0
      (let ((c 0))
        (dolist (x seq)
          (if (eql e x)
              (setq c (1+ c))))
        c)))