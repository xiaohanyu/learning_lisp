;; note that (listp nil) ==> T

(defun has-list-elt (x)
  (if (null x)
      nil
      (if (listp (car x))
          t
          (has-list-elt (cdr x)))))