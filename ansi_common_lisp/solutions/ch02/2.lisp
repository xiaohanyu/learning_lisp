;; method 1
(cons 'a (cons 'b (cons 'c nil)))

;; method 2
(cons 'a (cons 'b (cons 'c ())))

;; method 3
(cons 'a '(b c))