;;; 5.a 
;; returns t if x itself is null or x has an element of nil

(defun enigma (x)
  (and (not (null x))
            (or (null (car x))
                (enigma (cdr x)))))

;;; 5.b 
;; returns the first place of x in y, (index + 1)

(defun mystery (x y)
  (if (null y)
      nil
      (if (eql (car y) x)
          0
          (let ((z (mystery x (cdr y))))
            (and z (+ z 1))))))