;; the simple max function accept only two parameters

(defun mymax (a b)
  (if (> a b)
      a
      b))