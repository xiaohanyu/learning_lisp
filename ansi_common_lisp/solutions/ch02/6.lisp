;;; 6.a
;; x ==> car
(car (car (cdr '(a (b c) d))))

;;; 6.b 
;; x ==> or
(or 13 (/ 1 0))

;;; 6.c
;; x ==> apply
(x #'list 1 nil)