(defun compress (lst)
  (if (< (length lst) 2)
      lst
      (compr (car lst) 1 (cdr lst))))

(defun n-elts (n elts)
  (if (> n 1)
      ;; use dotted list (cons n elts) instead of normal list (list n elts)
      (cons n elts)
      elts))

(defun compr (elt n lst)
  (if (null lst)
      (list (n-elts n elt))
      (let ((next (car lst)))
        (if (eql elt next)
            (compr elt (+ n 1) (cdr lst))
            (cons (n-elts n elt)
                  (compr next 1 (cdr lst)))))))

(defun uncompress (lst)
  (if (null lst)
      nil
      (let ((first-elt (car lst))
            (rest-elts (uncompress (cdr lst))))
        (if (consp first-elt)            
            (append (apply #'list-of
                           ;; since we use dotted-list, we must construct a normal list, then pass it to list-of
                           (list (car first-elt) (cdr first-elt))) 
                    rest-elts)
            (cons first-elt rest-elts)))))

(defun list-of (n elt)
  (if (< n 1)
      nil
      (cons elt (list-of (- n 1) elt))))