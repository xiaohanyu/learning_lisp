;; combine the power of:
;; 1. alist
;; 2. sort with lambda function
;; 3. push and incf macro

(defun occurrences (lst)
  (let ((occur))
    (dolist (e lst)
      (if (assoc e occur)
          (incf (cdr (assoc e occur)))
          (push (cons e 1) occur)))
    (sort occur #'(lambda (x y) (> (cdr x) (cdr y))))))