;;; (a)
(defun my-cdr (x)
  (car x))

(defun my-car (x)
  (cdr x))

(defun my-cons (x y)
  (cons x y))

(setf x '(a b))
(setf y '(c d e))

(my-cons x y)
(my-cdr (my-cons x y))
(my-car (my-cons x y))

;; this is all the requirement that our cons/car/cdr should meet
;; ==> T
(eql (my-cdr (my-cons x y))
     x)
;; ==> T
(eql (my-car (my-cons x y))
     y)

;;; (b)
(defun our-list (&rest arg)
  (if (null arg)
      nil
      (my-cons (my-cdr arg)
               ;; using apply, otherwise, stack overflow
               (apply #'our-list (my-car arg)))))

(our-list)		;; ==> nil
(our-list 1)		;; ==> (1)
(our-list 1 2)		;; ==> (1 2)
(our-list 1 2 '(3 4))	;; ==> (1 2 (3 4))

;;; (c)
(defun my-length (lst)
  (if (null lst)
      0
      (+ 1 (my-length (my-car lst)))))

(my-length '(1 2 (3 4 (5 6)) 7))	;; ==> 4


;;; (d)
(defun my-member (obj lst)
  (if (eql obj (my-cdr lst))
      lst
      (my-member obj (my-car lst))))

(my-member 'a '(b (c d) a e (f g)))	;; ==> (a e (f g))
