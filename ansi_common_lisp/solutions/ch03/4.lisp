;; since the default test function of member is eql, two objects are eql when they are exactly the same object, not just the same value
;; for example
; (eql '(a) '(a)) ==> nil
; (eql (car '(a)) (car '(a))) ==> t