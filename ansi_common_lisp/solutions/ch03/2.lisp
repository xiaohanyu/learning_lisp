;; an ugly method

(defun new-union (x y &key (test #'eql test-supplied-p))
  (let ((myunion (reverse x)))
    (dolist (e y)
      (if (not (member e x :test test))
          (push e myunion)))
    (nreverse myunion)))