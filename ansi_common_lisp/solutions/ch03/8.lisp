(defun showdots (lst)
  (if (atom lst)
      (format t "~A" lst)
      (progn
        (format t "(") 
        (showdots (car lst))
        (format t " . ")
        (showdots (cdr lst))
        (format t ")"))))