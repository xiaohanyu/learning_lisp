;;; the recursive method
;; it need a helper recursive function, the recur-pos+ itself is used as a driver function
;; and the helper function is tail recursive.
(defun recur-pos+ (lst)
  (reverse (recur-pos+-helper lst 0 nil)))

(defun recur-pos+-helper (lst n return-lst)
  (if (zerop (length lst))
      return-lst
      (progn
        (push (+ (car lst) n) return-lst)
        (recur-pos+-helper (cdr lst) (+ n 1) return-lst))))

;;; the iterative method
;; a little ugly method
(defun iter-pos+ (lst)
  (let ((return-lst (copy-list lst)))
    (dotimes (i (length return-lst))
      (setf (nth i return-lst) (+ i (nth i return-lst))))
    return-lst))


;;; the mapcar method
;; we construct a helper list with contains (0 1 2 3 .. (- (length lst) 1))
;; then mapcar #+ with the original list and the helper list
(defun mapcar-pos+ (lst)
  (mapcar #'+
          lst
          (nreverse
           (let ((n-lst))
             (dotimes (i (length lst))
               (push i n-lst))
             n-lst))))
          