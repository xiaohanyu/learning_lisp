;;; define a single recursive function that returns, as two values, the maximum
;;; and minimum elements of a vector

(defun minmax (vec)
  (and (not (eql (length vec) 0))	;; if vec is empty, return nil
       (let ((v0 (aref vec 0)))
         (if (eql (length vec) 1)
             (values v0 v0)
             (multiple-value-bind (max min)
                 (minmax (subseq vec 1))
               (cond ((and (> max v0) (< min v0))
                      (values max min))
                     ((and (> max v0) (>= min v0))
                      (values max v0))
                     ((and (<= max v0) (< min v0))
                      (values v0 min))
                     ((and (<= max v0) (>= min v0))
                      (values v0 v0))))))))

(minmax '#(1 2 3 4 5))		;; => 5 1
(minmax '#(8 2 1 100 -10 9 23 1234 -32))	;; => 1234 -32
(minmax '#(1))			;; => 1 1
(minmax '#())			;; => nil
