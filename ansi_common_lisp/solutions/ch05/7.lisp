;;; Define a function that takes a list of numbers and returns true iff the
;;; difference between each successive pair of them is 1, using
;;; (a) recursion
;;; (b) do
;;; (c) mapc and return

;;; (a) the recursive method
(defun diff1 (lst)
  (cond ((or (not (listp nil))
             (null lst)
             (eql (length lst) 1))
         nil)
        ((and (eql (length lst) 2)
              (eql (abs (- (car lst)
                           (cadr lst)))
                   1))
         t)
        (t (and (eql (abs (- (car lst)
                             (cadr lst)))
                     1)
                (diff1 (cdr lst))))))

;;; (b) the iterative method using do
;;; a little ugly method
(defun diff1 (lst)
  (if (or (not (listp lst))
          (null lst)
          (eql (length lst) 1))
      nil
      (do ((i 0 (+ i 1))
           (j 1 (+ j 1)))
          ((= j (length lst)) (= j (length lst)))
        ;; (format t "~A - ~A~%" (elt lst i) (elt lst j))
        (if (not (eql (abs (- (elt lst i)
                              (elt lst j)))
                      1))
            (return nil)
            t))))

;;; (c) using mapc and return
(defun diff1 (lst)
  (if (or (not (listp lst))
          (null lst)
          (eql (length lst) 1))
      nil
      (progn
        (mapc #'(lambda (x y)
                  (if (not (eql (abs (- x y))
                                1))
                      (return nil)))
              lst
              (cdr lst))
        t)))

(diff1 '())			;; => nil
(diff1 '(1))			;; => nil
(diff1 '(1 2 3 4 5 6))		;; => t
(diff1 '(1 2 3 2 1))		;; => t
(diff1 '(1 2 3 4 6 5 4))	;; => nil
