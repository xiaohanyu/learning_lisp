;;; the recursive method
;;; using adjoin instead of cons
(defun precedes (ch vec)
  (cond ((eql (length vec) 0) nil)
        ((and (>= (length vec) 2)
              (eql (aref vec 1) ch))
         ;; using adjoin instead of cons to get rid of duplicates 
         (adjoin (aref vec 0)	
                 (precedes ch (subseq vec 1))))
        (t
         (precedes ch (subseq vec 1)))))

;;; the iterative method
(defun precedes (ch vec)
  (let ((lst))
    (cond ((eql (length vec) 0) nil)
          (t
           (do ((i 0 (+ i 1))
                (j 1 (+ j 1)))
               ((= j (length vec)) 'done)
             (if (eql (aref vec j) ch)
                 (setf lst
                       (adjoin (aref vec i)
                               lst))))))
    lst))
    
;;; test
(precedes #\a "")		;; => nil
(precedes #\a "a")		;; => nil
(precedes #\a "abracadabra")	;; => (#\c #\d #\r)
(precedes #\c "abracbracecfc")	;; => (#\f #\e #\a)
