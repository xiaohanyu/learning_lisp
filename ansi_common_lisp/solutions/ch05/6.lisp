;; the recursive method
(defun intersperse (delim lst)
  (if (or (null lst)
          (eql (length lst) 1))         
      lst
      (cons (car lst)
            (cons delim
                  (intersperse delim
                               (cdr lst))))))

;; the iterative method
;; not as elegant as the recursive method
(defun intersperse (delim lst)
  (let ((newlst))
    (cond ((or (null lst)
               (eql (length lst) 1))
           (setf newlst lst))
          (t
           (push (car lst) newlst)
           (dolist (obj (cdr lst))
             (push delim newlst)
             (push obj newlst))))
    (reverse newlst)))

;; test
(intersperse '- '(a b c d))	;; => (A - B - C - D)
(intersperse '- '(a))		;; => (A)
(intersperse '- 'nil)		;; => nil
