(defun 4th (lst)
  (car (cdr (cdr (cdr lst)))))

;; ==> d
(4th '(a b c d e f))

;; ==> 4
(4th '(1 2 3 4 5 6))

;; ==> 5
(nth 4 '(1 2 3 4 5 6))
