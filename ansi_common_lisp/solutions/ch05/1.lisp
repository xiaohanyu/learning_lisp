;;; (a)

(setf y '((a b) c))

(let ((x (car y)))
  (cons x x))

;; equivalent to 
((lambda (x)
   (cons x x))
 (car y))

;;; (b)
(setf x '(1 2))
(setf z 3)

;; ==> (1 . 4)
(let* ((w (car x))
       (y (+ w z)))
  (cons w y))

;; equivalent to
;; ==> (1 . 4)
((lambda (w)
   (cons w
         ((lambda (y)
            y)
          (+ w z))))
 (car x))
