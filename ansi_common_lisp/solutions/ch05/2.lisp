;; using if
;; what mystery really do is like find, except that mystery return the index,
;; while find returns the object itself
(defun mystery (x y)
  (if (null y)
      nil
      (if (eql (car y) x)
          0
          (let ((z (mystery x (cdr y))))
            (and z (+ z 1))))))

;; ==> c
(find 'c '(b a (a b c) c))

;; ==> 3
(mystery 'c '(b a (a b c) c))

;; using cond instead of if
(defun mystery (x y)
  (cond ((null y)
         nil)
        ((eql (car y) x)
         0)
        (t
         (let ((z (mystery x (cdr y))))
           (and z (+ z 1))))))
