;; easy problem
(defun nondecrese (lst)
  (and (every #'floatp lst)
       (apply #'< lst)))

;; test
;; CL-USER> (nondecrese '(1.0 2.0 3.0 4.0))
;; T
;; CL-USER> (nondecrese '(1.0 2.0 3.0 4))
;; NIL
