;; SBCL and CCL has only SINGLE-FLOAT and DOUBLE-FLOAT 
(type-of 1.0s10)
(type-of 1.0f10)
(type-of 1.0d10)
(type-of 1.0l10)

;; in ecl
;; > (type-of 1.0s10)

;; SINGLE-FLOAT
;; > (type-of 1.0d10)

;; DOUBLE-FLOAT
;; > (type-of 1.0f10)

;; SINGLE-FLOAT
;; > (type-of 1.0l10)

;; LONG-FLOAT
;; > 
