;; use the powerful reduce function 
(defun horner-rule (x &rest coeffs)
  (reduce #'(lambda (a b)
              (+ (* a x) b))
          coeffs))

;; use recursion
(defun horner-rule (x &rest coeffs)
  (defun horner-helper (x coeff-rev-lst)
    (let ((len (length coeff-rev-lst)))
      (cond ((= len 1)
             (car coeff-rev-lst))
            ((= len 2)
             (+ (car coeff-rev-lst)
                (* (cadr coeff-rev-lst) x)))
            (t
             (+ (car coeff-rev-lst)
                (* x (horner-helper x (cdr coeff-rev-lst))))))))
  (horner-helper x (reverse coeffs)))

;; use iteration
(defun horner-rule (x &rest coeffs)
  (let ((sum 0))
    (dolist (c coeffs)
      (setf sum (+ (* sum x) c)))
    sum))

(horner-rule 3 1)		;; => 1
(horner-rule 3 1 2)		;; => 5
(horner-rule 3 2 0 1)		;; => 19
(horner-rule 3 2 0 3)		;; => 21
(horner-rule 5 2 2 3)		;; => 63
