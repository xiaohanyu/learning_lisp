;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; from Tsinghua's ACM templates                                          ;;
;;                                                                           ;;
;; point intersection(line u,line v){                                        ;;
;;     point ret=u.a;                                                        ;;
;;     double t=((u.a.x-v.a.x)*(v.a.y-v.b.y)-(u.a.y-v.a.y)*(v.a.x-v.b.x))    ;;
;;         /((u.a.x-u.b.x)*(v.a.y-v.b.y)-(u.a.y-u.b.y)*(v.a.x-v.b.x));       ;;
;;     ret.x+=(u.b.x-u.a.x)*t;                                               ;;
;;     ret.y+=(u.b.y-u.a.y)*t;                                               ;;
;;     return ret;                                                           ;;
;; }                                                                         ;;
;;                                                                           ;;
;; point intersection(point u1,point u2,point v1,point v2){                  ;;
;;     point ret=u1;                                                         ;;
;;     double t=((u1.x-v1.x)*(v1.y-v2.y)-(u1.y-v1.y)*(v1.x-v2.x))            ;;
;;         /((u1.x-u2.x)*(v1.y-v2.y)-(u1.y-u2.y)*(v1.x-v2.x));               ;;
;;     ret.x+=(u2.x-u1.x)*t;                                                 ;;
;;     ret.y+=(u2.y-u1.y)*t;                                                 ;;
;;     return ret; 35                                                        ;;
;;                     }                                                     ;;
;;                                                                           ;;
;;                                                                           ;;
;; int parallel(line u,line v){                                              ;;
;;     return zero((u.a.x-u.b.x)*(v.a.y-v.b.y)-(v.a.x-v.b.x)*(u.a.y-u.b.y)); ;;
;; }                                                                         ;;
;;                                                                           ;;
;; int parallel(point u1,point u2,point v1,point v2){                        ;;
;;     return zero((u1.x-u2.x)*(v1.y-v2.y)-(v1.x-v2.x)*(u1.y-u2.y));         ;;
;; }                                                                         ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun line-intersect (u1x u1y u2x u2y v1x v1y v2x v2y)
  (and (> (- (* (- u1x u2x)
                (- v1y v2y))
             (* (- v1x v2x)
                (- u1y u2y))) 0.0001)
       (let* ((ts (/ (- (* (- u1x v1x)
                           (- v1y v2y))
                        (* (- u1y v1y)
                           (- v1x v2x)))
                     (- (* (- u1x u2x)
                           (- v1y v2y))
                        (* (- u1y u2y)
                           (- v1x v2x)))))
              (retx (+ u1x (* (- u2x u1x) ts)))
              (rety (+ u1y (* (- u2y u1y) ts))))
         (and (<= u1x retx) (<= retx u2x)
              (<= u1y rety) (<= rety u2y)
              (values retx rety)))))

(line-intersect 0 0 4 4 4 0 0 4)	;; => 2, 2
(line-intersect 0 0 4 4 1 0 3 4)	;; => 2, 2
(line-intersect 0 0 1 4 4 0 0 4)	;; => 4/5, 16/5
(line-intersect 0 0 4 4 1 0 5 4)	;; => NIL
(line-intersect 0 0 4 4 1 0 40/39 4)	;; => 156/155 156/155
